TSUtils -- Name change pending

TsUtils is a javascript framework built for Node.js platform. It is inspired by MooTool's OOPS concept and draws heavily from it. 
It provides a simple yet powerfull middle layer on which applications can be built.

Structure
-- Root Folder
  |
  -- Application
    |
    -- Libraries
    |
    -- Models
    |
    -- Views
    |
    -- Skins
    |
    -- Controller
    |
    -- Logs
    |
    -- Cache
    |
    -- Core.js
  |
  -- index.js

index.js is the main js file which is called to start the application

Core.js contains the actual heart of the system. The entire bootstrap process is handled here.

Libraries are Helper classes such as File Access class etc which wrap around the actual Node.js class. This is done so that the naming convention is followed in the application. Libraries can be written by any one and loaded with the simple require function of node.js