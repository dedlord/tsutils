;(function(Global)
    {
        "use strict";
        
        var TsUtils = Global.TsUtils = {
                Version: '0.0.1', 
                Build: 'alpha'
            }, 
            
            // Default Functions
            DefaultImplements = {
                
                String: ['charAt', 
                            'charCodeAt', 
                            'concat', 
                            'contains', 
                            'indexOf', 
                            'lastIndexOf', 
                            'match', 
                            'quote', 
                            'replace', 
                            'search', 
                            'slice', 
                            'split', 
                            'substr', 
                            'substring', 
                            'trim', 
                            'toLowerCase', 
                            'toUpperCase'], 
                
                Number: ['toExponential', 
                            'toFixed', 
                            'toLocaleString', 
                            'toPrecision'], 
                
                Array: ['pop', 
                        'push', 
                        'reverse', 
                        'shift', 
                        'sort', 
                        'splice', 
                        'unshift', 
                        'concat', 
                        'join', 
                        'slice',
                        'indexOf', 
                        'lastIndexOf', 
                        'filter', 
                        'forEach', 
                        'every', 
                        'map', 
                        'some', 
                        'reduce', 
                        'reduceRight'], 
                
                Function: ['apply', 
                            'call', 
                            'bind'], 
                
                Object: ['create', 
                            'defineProperty', 
                            'defineProperties', 
                            'keys',
                            'getPrototypeOf', 
                            'getOwnPropertyDescriptor', 
                            'getOwnPropertyNames', 
                            'preventExtensions', 
                            'isExtensible', 
                            'seal', 
                            'isSealed', 
                            'freeze', 
                            'isFrozen'], 
                
                RegExp: ['exec', 
                            'test']
            }, 
            
            // List of Extensions for Native Types
            Extends = {
                
                String: {
                    
                }, 
                
                Number: {
                    
                }, 
                
                Array: {
                    From: function(Item)
                        {
                            if(Item === null || Item === undefined)
                                {
                                    return [];
                                }
                            return (GetType(Item) === 'array') ? Item : [Item];
                        }
                }, 
                
                Function: {
                    Attempt: function()
                        {
                            
                        }
                    attempt: function(){
		for (var i = 0, l = arguments.length; i < l; i++){
			try {
				return arguments[i]();
			} catch (e){}
		}
		return null;
	}
                }, 
                
                Object: {
                    Merge: function(Source, Key, Value)
                        {
                            if(GetType(Key) === 'string')
                                {
                                    return MergeOne(Source, Key, Value);
                                }
                            for (var I = 1, Len = arguments.length; I < Len; I += 1)
                                {
                                    var Obj = arguments[I];
                                    for (var K in Obj)
                                        {
                                            MergeOne(Source, K, Obj[K]);
                                        }
                                }
                            return Source;
                        }, 
                    Clone: function(Original)
                        {
                            var Clone = {};
                            for (var Key in Original)
                                {
                                    Clone[Key] = CloneOf(Original[Key]);
                                }
                            return Clone;
                        }, 
                    Append: function(Original)
                        {
                            for (var I = 1, Len = arguments.length; I < Len; I += 1)
                                {
                                    var Extended = arguments[I] || {};
                                    for (var Key in Extend)
                                        {
                                            Original[Key] = Extended[Key];
                                        }
                                }
                            return Original;
                        }, 
                    GetFromPath: function(Source, Parts)
                        {
                            var HasOwn = Object.prototype.hasOwnProperty;
                            if(GetType(Parts) === 'string')
                                {
                                    Parts = Parts.split('.');
                                }
                            Parts.Each(function(Part)
                                {
                                    if(HasOwn.call(Source, Part))
                                        {
                                            Source = Source[Part];
                                        }
                                    return null;
                                }, this);
                            return Source;
                        }, 
                    cleanValues: function(object, method){
		method = method || defined;
		for (var key in object) if (!method(object[key])){
			delete object[key];
		}
		return object;
	},

	erase: function(object, key){
		if (hasOwnProperty.call(object, key)) delete object[key];
		return object;
	},

	run: function(object){
		var args = Array.slice(arguments, 1);
		for (var key in object) if (object[key].apply){
			object[key].apply(object, args);
		}
		return object;
	},  
	               subset: function(object, keys){
		var results = {};
		for (var i = 0, l = keys.length; i < l; i++){
			var k = keys[i];
			if (k in object) results[k] = object[k];
		}
		return results;
	},

	map: function(object, fn, bind){
		var results = {};
		for (var key in object){
			if (hasOwnProperty.call(object, key)) results[key] = fn.call(bind, object[key], key, object);
		}
		return results;
	},

	filter: function(object, fn, bind){
		var results = {};
		for (var key in object){
			var value = object[key];
			if (hasOwnProperty.call(object, key) && fn.call(bind, value, key, object)) results[key] = value;
		}
		return results;
	},

	every: function(object, fn, bind){
		for (var key in object){
			if (hasOwnProperty.call(object, key) && !fn.call(bind, object[key], key)) return false;
		}
		return true;
	},

	some: function(object, fn, bind){
		for (var key in object){
			if (hasOwnProperty.call(object, key) && fn.call(bind, object[key], key)) return true;
		}
		return false;
	},

	keys: function(object){
		var keys = [];
		for (var key in object){
			if (hasOwnProperty.call(object, key)) keys.push(key);
		}
		return keys;
	},

	values: function(object){
		var values = [];
		for (var key in object){
			if (hasOwnProperty.call(object, key)) values.push(object[key]);
		}
		return values;
	},

	getLength: function(object){
		return Object.keys(object).length;
	},

	keyOf: function(object, value){
		for (var key in object){
			if (hasOwnProperty.call(object, key) && object[key] === value) return key;
		}
		return null;
	},

	contains: function(object, value){
		return Object.keyOf(object, value) != null;
	},

	toQueryString: function(object, base){
		var queryString = [];

		Object.each(object, function(value, key){
			if (base) key = base + '[' + key + ']';
			var result;
			switch (typeOf(value)){
				case 'object': result = Object.toQueryString(value, key); break;
				case 'array':
					var qs = {};
					value.each(function(val, i){
						qs[i] = val;
					});
					result = Object.toQueryString(qs, key);
				break;
				default: result = key + '=' + encodeURIComponent(value);
			}
			if (value != null) queryString.push(result);
		});

		return queryString.join('&');
	}
                }, 
                
                Boolean: {
                    
                }, 
                
                RegExp: {
                    
                }
            }, 
            
            // List of Implemnts for Native Types
            Implements = {
                
                String: {
                    Test: function(Regex, Params)
                        {
                            return ((GetType(Regex) === 'regexp') ? Regex : new RegExp('' + Regex, Params)).test(this);
                        }, 
                    Trim: function(Char)
                        {
                            return String(this).replace(/^\s+|\s+$/g, '');
                        }, 
                    TrimLeft: function(Char)
                        {
                            
                        }, 
                    TrimRight: function(Char)
                        {
                            
                        }, 
                    Clean: function()
                        {
                            return String(this).replace(/\s+/g, ' ').Trim();
                        }, 
                    CamelCase: function()
                        {
                            return String(this).replace(/-\D/g, function(Match)
                                {
                                    return Match.charAt(1).toUpperCase();
                                });
                        }, 
                    Hyphenate: function()
                        {
                            return String(this).replace(/[A-Z]/g, function(Match)
                                {
                                    return ('-' + Match.charAt(1).toLowerCase());
                                });
                        }, 
                    Capitalize: function()
                        {
                            return String(this).replace(/\b[a-z]/g, function(Match)
                                {
                                    return Match.toUpperCase();
                                });
                        }, 
                    EscapeRegexp: function()
                        {
                            return String(this).replace(/([-.*+?^${}()|[\]\/\\])/g, '\\$1');
                        }, 
                    ToInt: function()
                        {
                            return parseInt(this, 10);
                        }, 
                    ToOct: function()
                        {
                            return parseInt(this, 8);
                        }, 
                    ToBinary: function()
                        {
                            return parseInt(this, 2);
                        }, 
                    ToFloat: function()
                        {
                            return parseFloat(this);
                        }, 
                    ToHex: function()
                        {
                            var Hex = '';
                            for (var I = 0, Len = this.length; I < Len; I += 1)
                                {
                                    Hex += '' + this.charCodeAt(I).toString(16);
                                }
                            return Hex;
                        }, 
                    HexToInt: function()
                        {
                            return parseInt(this, 16);
                        }, 
                    Pad: function(Length, PadWith)
                        {
                            PadWith = PadWith || ' ';
                            Length = Length || 0;
                            var Str = this;
                            while(Str.length < Length)
                                {
                                    Str += PadWith;
                                }
                            return Str;
                        }, 
                    PadLeft: function(Length, PadWith)
                        {
                            PadWith = PadWith || ' ';
                            Length = Length || 0;
                            var Str = this.toString();
                            while(Str.length < Length)
                                {
                                    Str = PadWith + Str;
                                }
                            return Str;
                        }, 
                    Substitute: function(Obj, Regexp)
                        {
                            return String(this).replace(Regexp || (/\\?\{([^{}]+)\}/g), function(Match, Name)
                                {
                                    if(Match.charAt(0) === '\\')
                                        {
                                            return Match.slice(1);
                                        }
                                    return (Obj[Name] !== null) ? Obj[Name] : '';
                                });
                        } 
                }, 
                
                Number: {
                    ToHex: function()
                        {
                            return this.toString(16);
                        }, 
                    ToInt: function()
                        {
                            return parseInt(this, 10);
                        }, 
                    ToOct: function()
                        {
                            return parseInt(this, 8);
                        }, 
                    ToBinary: function()
                        {
                            return parseInt(this, 2);
                        }, 
                    ToFloat: function()
                        {
                            return parseFloat(this);
                        }, 
                    Limit: function(Min, Max)
                        {
                            return Math.min(Max, Math.max(Min, this));
                        }, 
                    Round: function(Precision)
                        {
                            Precision = Math.pow(10, Precision || 0).toFixed(Precision < 0 ? -Precision : 0);
                            return Math.round(this * Precision) / Precision;
                        }, 
                    Times: function(Fn, Bind)
                        {
                            for (var I = 0; I < this; I += 1)
                                {
                                    Fn.call(Bind, I, this);
                                }
                        }, 
                    /* Math Functions */
                    Abs: function()
                        {
                            return Math.abs(this);
                        }, 
                    Acos: function()
                        {
                            return Math.acos(this);
                        }, 
                    Asin: function()
                        {
                            return Math.asin(this);
                        }, 
                    Atan: function()
                        {
                            return Math.atan(this);
                        }, 
                    Atan2: function()
                        {
                            return Math.atan2(x, y)
                        }, 
                    Ceil: function()
                        {
                            return Math.ceil(this);
                        }, 
                    Cos: function()
                        {
                            return Math.cos(this);
                        }, 
                    Exp: function()
                        {
                            return Math.exp(this);
                        }, 
                    Floor: function()
                        {
                            return Math.floor(this);
                        }, 
                    Log: function()
                        {
                            return Math.log(this);
                        }, 
                    Sqrt: function()
                        {
                            return Math.sqrt(this);
                        }
                     //'atan2', 'max', 'min', 'pow', 'sin', 'tan'
                }, 
                
                Array: {
                    Clone: function()
                        {
                            var I = this.length, 
                                Clone = [];
                            while (I > -1)
                                {
                                    Clone[I] = CloneOf(this[I]);
                                    I -= 1;
                                }
                            return Clone;
                        }, 
                    ForEach: function(Fn, Bind)
                        {
                            for (var I = 0, Len = this.length; I < Len; I += 1)
                                {
                                    if( I in this)
                                        {
                                            Fn.call(Bind, this[I], I, this);
                                        }
                                }
                        }, 
                    Each: function(Fn, Bind)
                        {
                            for (var I = 0, Len = this.length; I < Len; I += 1)
                                {
                                    if( I in this)
                                        {
                                            Fn.call(Bind, this[I], I, this);
                                        }
                                }
                        }, 
                    Clean: function()
                        {
                            return this.Filter(function(Item)
                                {
                                    return Item != false;
                                });
                        }, 
                    Invoke: function(Method)
                        {
                            var Args = Array.slice(arguments, 1);
                            return this.Map(function(Item)
                                {
                                    return Item[Method].apply(Item, Args);
                                })l
                        }, 
	associate: function(keys){
		var obj = {}, length = Math.min(this.length, keys.length);
		for (var i = 0; i < length; i++) obj[keys[i]] = this[i];
		return obj;
	},

	link: function(object){
		var result = {};
		for (var i = 0, l = this.length; i < l; i++){
			for (var key in object){
				if (object[key](this[i])){
					result[key] = this[i];
					delete object[key];
					break;
				}
			}
		}
		return result;
	},

	contains: function(item, from){
		return this.indexOf(item, from) != -1;
	},

	append: function(array){
		this.push.apply(this, array);
		return this;
	},

	getLast: function(){
		return (this.length) ? this[this.length - 1] : null;
	},

	getRandom: function(){
		return (this.length) ? this[Number.random(0, this.length - 1)] : null;
	},

	include: function(item){
		if (!this.contains(item)) this.push(item);
		return this;
	},

	combine: function(array){
		for (var i = 0, l = array.length; i < l; i++) this.include(array[i]);
		return this;
	},

	erase: function(item){
		for (var i = this.length; i--;){
			if (this[i] === item) this.splice(i, 1);
		}
		return this;
	},

	empty: function(){
		this.length = 0;
		return this;
	},

	flatten: function(){
		var array = [];
		for (var i = 0, l = this.length; i < l; i++){
			var type = typeOf(this[i]);
			if (type == 'null') continue;
			array = array.concat((type == 'array' || type == 'collection' || type == 'arguments' || instanceOf(this[i], Array)) ? Array.flatten(this[i]) : this[i]);
		}
		return array;
	},

	pick: function(){
		for (var i = 0, l = this.length; i < l; i++){
			if (this[i] != null) return this[i];
		}
		return null;
	}, 
	min: function(){
		return Math.min.apply(null, this);
	},

	max: function(){
		return Math.max.apply(null, this);
	},

	average: function(){
		return this.length ? this.sum() / this.length : 0;
	},

	sum: function(){
		var result = 0, l = this.length;
		if (l){
			while (l--){
				if (this[l] != null) result += parseFloat(this[l]);
			}
		}
		return result;
	},

	unique: function(){
		return [].combine(this);
	},

	shuffle: function(){
		for (var i = this.length; i && --i;){
			var temp = this[i], r = Math.floor(Math.random() * ( i + 1 ));
			this[i] = this[r];
			this[r] = temp;
		}
		return this;
	},

	reduce: function(fn, value){
		for (var i = 0, l = this.length; i < l; i++){
			if (i in this) value = value === nil ? this[i] : fn.call(null, value, this[i], i, this);
		}
		return value;
	},

	reduceRight: function(fn, value){
		var i = this.length;
		while (i--){
			if (i in this) value = value === nil ? this[i] : fn.call(null, value, this[i], i, this);
		}
		return value;
	},

	pluck: function(prop){
		return this.map(function(item){
			return item[prop];
		});
	}
                }, 
                
                Function: {
                    Attempt: function(Args, Bind)
                        {
                            try
                                {
                                    return this.apply(Bind, Array.From(Args));
                                }
                            catch (e) {}
                            return null;
                        }, 
                    Pass: function(Args, Bind)
                        {
                            var Self = this;
                            if(Args !== null)
                                {
                                    args = Array.From(Args);
                                }
                            return function()
                                {
                                    Self.apply(Bind, Args || arguments);
                                };
                        }, 
                    Delay: function(Delay, Bind, Args)
                        {
                            Delay = parseInt(Delay, 10) || 0;
                            return setTimeout(this.Pass((), Bind), Delay);
                        }, 
                    Periodical: function(Timeout, Bind, Args)
                        {
                            Timeout = parseInt(Delay, 10) || 0;
                            return setInterval(this.Pass((), Bind), Timeout);
                        }
                }, 
                
                Object: {
                    
                }, 
                
                Boolean: {
                    
                }, 
                
                RegExp: {
                    
                }
            }, 
            
            GetType = Global.GetType = function(Item)
                {
                    if(Item === undefined)
                        {
                            return 'undefined';
                        }
                    if(Item === null)
                        {
                            return 'null';
                        }
                    if(Item.$Family)
                        {
                            return Item.$Family();
                        }
                    else if(typeof Item.length === 'number')
                        {
                            if('callee' in Item)
                                {
                                    return 'arguments';
                                }
                            else if('item' in Item)
                                {
                                    return 'collection';
                                }
                        }
                    return typeof Item;
                }, 
            
            OverloadSetter = function()
                {
                    var Self = this;
                    return function()
                        {
                            var Key = arguments[0] || null, 
                                Value = arguments[1] || null, 
                                K;
                            if(Key !== null)
                                {
                                    if(typeof Key !== 'string')
                                        {
                                            for (K in Key)
                                                {
                                                    Self.call(this, K, Key[K]);
                                                }
                                        }
                                    Self.call(this, Key, Value);
                                }
                            return this;
                        };
                }, 
            
            OverloadGetter = function()
                {
                    var Self = this;
                    return function()
                        {
                            var Args = arguments, 
                                Arg, 
                                Result;
                            if(Args.length > 1)
                                {
                                    Result = {};
                                    for (Arg in Args)
                                        {
                                            Result[Arg] = self.call(this, Arg);
                                        }
                                }
                            else
                                {
                                    Result = self.call(this, Args[0]);
                                }
                            return Result;
                        };
                }, 
                
            Extend = function(Key, Value)
                {
                    if(Value && !Value.$Hidden)
                        {
                            var Previous = this[Key];
                            if(Previous === undefined || (Previous !== undefined && !Previous.$Protected))
                                {
                                    this[Key] = Value;
                                }
                        }
                    return this;
                }, 
            
            Implement = function(Key, Value)
                {
                    if(Value && !Value.$Hidden)
                        {
                            var Previous = this.prototype[Key];
                            if(Previous === undefined || (Previous !== undefined && !Previous.$Protected))
                                {
                                    this.prototype[Key] = Value;
                                }
                        }
                    return this;
                }, 
            
            ClassImplement = function(Key, Value, Retain)
                {
                    Retain = Retain || false;
                    if(Key === 'Implements')
                        {
                            Array.From(Value).Each(function(Val)
                                {
                                    var Instance = new Val;
                                    for (var K in Instance)
                                        {
                                            ClassImplement.call(this, K, Instance[K], true);
                                        }
                                }, this);
                        }
                    else if(Key === 'Extends')
                        {
                            this.Parent = Value;
                            this.prototype = new GetInstance(Value);
                        }
                    if(GetType(Value) === 'function')
                        {
                            if (!Value.$Hidden)
                                {
                                    this.prototype[Key] = (Retain === true) ? Value : Wrap(this, Key, Value);
                                }
                        }
                    else if(Value !== undefined || Value !== null)
                        {
                            Object.Merge(this.prototype, Key, Value);
                        }
                    return this;
                }, 
            
            Wrap = function(Self, Key, Method)
                {
                    if(Method.$Origin)
                        {
                            Method = Method.$Origin;
                        }
                    var Wrapper = function()
                        {
                            if(Method.$Protected && this.$Caller === null)
                                {
                                    throw new Error('The Method "' + Key + '" cannot be called');
                                }
                            var Caller = this.Caller, 
                                Current = this.$Caller, 
                                Result = Method.apply(this, arguments);
                            this.$Caller = Current;
                            this.Caller = Caller;
                            return Result;
                        }.Extend({$Owner: Self, 
                                    $Origin: Method, 
                                    $Name: Key});
                },
            
            Hide = function()
                {
                    this.$Hidden = true;
                    return this;
                }, 
            
            Protect = function()
                {
                    this.$Protected = true;
                    return this;
                }, 
            
            Alias = function(New, Old)
                {
                    Implement.call(this, New, this.prototype[Old]);
                }, 
            
            Type = function(Name, Obj)
                {
                    if(Name)
                        {
                            var TypeLower = Name.toLowerCase();
                            if(Obj !== null)
                                {
                                    Obj.prototype.$Family = (function()
                                        {
                                            return TypeLower;
                                        }).Hide();
                                    
                                    Obj.Extend(this);
                                    Obj.$Constructor = Type;
                                    Obj.prototype.$Constructor = Obj;
                                    return Obj;
                                }
                            return null;
                        }
                }, 
            
            LoadType = function(Name, Obj, Methods)
                {
                    var IsType = (Obj !== Object), 
                        Prototype = Obj.prototype;
                    
                    if(IsType)
                        {
                            Obj = new Type(Name, Obj);
                        }
                    for (var I = 0, Len = Methods.length; I < Len; I+= 1)
                        {
                            var Key = Methods[I], 
                                Generic = Obj[Key], 
                                Proto = Prototype[Key];
                            if(Generic)
                                {
                                    Generic.Protect();
                                }
                            if(IsType && Proto)
                                {
                                    Obj.Implement(Key, Proto.Protect());
                                }
                        }
                    Obj.Extend(Extends[Name]);
                    Obj.Implement(Implements[Name]);
                }, 
            
            MergeOne = function(Source, Key, Current)
                {
                    switch (GetType(Current))
                        {
                            case 'object': 
                                if(GetType(Source[Key]) === 'object')
                                    {
                                        Object.Merge(Source[Key], Current);
                                    }
                                else
                                    {
                                        Source[Key] = Object.Clone(Current);
                                    }
                                break;
                            case 'array': 
                                Source[Key] = Current.Clone();
                                break;
                            default: 
                                Source[Key] = Current;
                                break;
                        }
                    return Source;
                }, 
            
            CloneOf = function(Item)
                {
                    switch (GetType(Item))
                        {
                            case 'array': 
                                return Item.Clone();
                            case 'object': 
                                return Item.Clone();
                            default: 
                                return Item;
                        }
                };

        // We need to add these into functions even before we proceed
        Function.prototype.Hide = Hide;
        Function.prototype.Protect = Protect;
        Function.prototype.OverloadSetter = OverloadSetter;
        Function.prototype.OverloadSetter = OverloadSetter;
        Function.prototype.Implement = Implement.OverloadSetter();
        Function.prototype.Extend = Extend.OverloadSetter();
        Object.prototype.Merge = Extends.Object.Merge;
        
        Type.Implement({
            Implement: Implement.OverloadSetter(), 
            
            Extend: Extend.OverloadSetter(), 
            
            Alias: Alias.OverloadSetter()
        });
        
        new Type('Type', Type);
        
        /* Load Default Types */
        new LoadType('String', String, DefaultImplements.String || []);
        new LoadType('Number', Number, DefaultImplements.Number || []);
        new LoadType('Boolean', Boolean, DefaultImplements.Boolean || []);
        new LoadType('Array', Array, DefaultImplements.Array || []);
        new LoadType('Object', Object, DefaultImplements.Object || []);
        new LoadType('RegExp', RegExp, DefaultImplements.RegExp || []);
        new LoadType('Date', Date, DefaultImplements.Date || []);
        new LoadType('Function', Function, DefaultImplements.Function || []);
        
        /* OOPS */
        var Class = Global.Class = new Type('Class', function(Params)
                {
                    //var Args = arguments;
                    if(GetType(Params) === 'function')
                        {
                            Params = {__Constructor: Params};
                        }
                    var NewClass = function()
                        {
                            Reset(this);
                            if(NewClass.$Prototyping)
                                {
                                    return this;
                                }
                            this.$Caller = null;
                            var Value = (this.__Constructor) ? this.__Constructor.apply(this, arguments) : this;
                            this.$Caller = this.$Caller = null;
                            return Value;
                        }.Extend(this).Implement(Params);
                    NewClass.$Constructor = Class;
                    NewClass.prototype.$Constructor = NewClass;
                    NewClass.prototype.Parent = function()
                        {
                            if(!this.$Caller)
                                {
                                    throw new Error('The method "Parent" cannot be called');
                                }
                            var Name = this.$Caller.$Name, 
                                Parent = this.$Caller.$Owner.Parent, 
                                Previous = (Parent) ? Parent.prototype[Name] : null;
                            if(!Previous)
                                {
                                    throw new Error('The method "' + Name + '" has no parent');
                                }
                            return Previous.apply(this, arguments);
                        };
                    return NewClass;
                }), 
            
            Reset = function(Obj)
                {
                    for(var Key in Obj)
                        {
                            var Value = this[Key];
                            switch(GetType(Value))
                                {
                                    case 'object': 
                                        var F = function(){};
                                        F.prototype = Value;
                                        Obj[Key] = new Reset(new F());
                                        break;
                                    case 'array': 
                                        Obj[Key] = Value.Clone();
                                        break;
                                }
                        }
                    return Obj;
                }, 
            
            GetInstance = function(TheClass)
                {
                    TheClass.$Prototyping = true;
                    var Proto = new TheClass;
                    delete TheClass.$Prototyping;
                    return Proto;
                };
        
        Class.Implement('Implement', ClassImplement.OverloadSetter());
        
        /* Helper Classes */
        var Options = Global.Options = new Class(
                {
                    SetOption: function(Name, Value)
                        {
                            if(Name.test(/^on[A-Z]/i) && this.$Events)
                                {
                                    this.On(Name, Value);
                                }
                            else if(this.Options[Name])
                                {
                                    this.Options[Name] = Value;
                                }
                            return this;
                        }.OverloadSetter(), 
                    
                    GetOption: function(Name)
                        {
                            return this.Options[Name];
                        }
                }), 
            
            Events = Global.Events = new Class(
                {
                    $Events: {}, 
                    
                    On: function(Name, CallBack)
                        {
                            Name = this.__RemoveOn(Name).toLowerCase();
                            this.$Events[Name] = (this.$Events[Name] || []).Include(CallBack);
                            return this;
                        }.OverloadSetter(), 
                    
                    Fire: function(Name, Args, Delay)
                        {
                            Name = this.__RemoveOn(Name).toLowerCase();
                            Delay = parseInt(Delay, 10) || 0;
                            var Events = this.$Events[Name];
                            if(Events !== undefined && Events.length > 0)
                                {
                                    Events.Each(function(CallBack)
                                        {
                                            if(Delay > 0)
                                                {
                                                    CallBack.Delay(Delay, this, Args);
                                                }
                                            else
                                                {
                                                    CallBack.Apply(this, Args);
                                                }
                                        }, this);
                                }
                            return this;
                        }, 
                    
                    __RemoveOn: function(Name)
                        {
                            return Name.replace(/^on([A-Z])/i, function(Full, TheName)
                                {
                                    return TheName.toLowerCase();
                                });
                        }.Protect()
                }), 
            
            Chain = Global.Chain = new Class(
                {
                    $Chain: [], 
                    
                    Add: function()
                        {
                            this.$Chain.append(Array.flatten(arguments));
                            return this;
                        }, 
                    
                    Clear: function()
                        {
                            this.$Chain.Empty();
                            return this;
                        }, 
                    
                    Call: function()
                        {  
                            return (this.$Chain.length) ? this.$Chain.shift().apply(this, arguments) : false;
                        }
                });
    })(global);