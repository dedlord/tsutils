// @TODO Code commenting & Test cases
;(function()
    {
        "use strict";
        
        var OTP = new Class(
            {
                Implements: [Options], 
                
                Options: {
                    // Pre-defined secret key
                    Secret: '', 
                    // The Counter (for HOTP)
                    Counter: 0, 
                    // Timeout for TOPT
                    Timeout: 30, 
                    // Mode, Supports TOTP/HOTP only
                    Mode: 'TOTP', 
                    // Number of digits to use, 6 is default 8 is max
                    Digits: 6
                }, 
                
                // Crypto library
                Crypt: require('crypto'), 
                
                __Constructor: function(Options)
                    {
                        this.SetOption(Options);
                    }, 
                
                Generate: function(Secret, Counter)
                    {
                        Secret = Secret || this.GetOption('Secret');
                        Counter = Counter || this.GetOption('Counter');
                        if(this.GetOption('Mode').toLowerCase() === 'totp')
                            {
                                Counter = Math.floor((new Date().getTime()) / (this.GetOption('Timeout') * 1000.0));
                            }
                        return this.__Generate(Secret, Counter);
                    }, 
                
                Verify: function(Code, Secret, Counter)
                    {
                        return (Code === this.Generate(Secret, Counter));
                    }, 
                
                CreateKey: function()
                    {
                        
                    }, 
                
                __Generate: function(Secret, Counter)
                    {
                        Secret = Secret.ToHex();
                        Counter = Counter.ToHex().PadLeft(16, '0');
                        var Crypt = this.Crypt.createHmac('sha1', new Buffer(Secret, 'hex')), 
                            HMAC = Crypt.update(new Buffer(Counter, 'hex'))
                                        .digest('hex'), 
                            Offset = HMAC.substr(HMAC.length-1).HexToInt(), 
                            Truncated = HMAC.substr(Offset*2, 8), 
                            SigBit0 = Truncated.HexToInt() & '7fffffff'.HexToInt(), 
                            Code = (SigBit0 % Math.pow(10, this.GetOption('Digits'))).toString().PadLeft(this.GetOption('Digits'), '0');
                        return Code;
                    }.Protect()
            });
        
        module.exports = OTP;
    })(this);