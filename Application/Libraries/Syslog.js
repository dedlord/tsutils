#!/usr/bin/env node
// @TODO: Finish Parser and Generator, Add Validators. Code comment and Test case
/*jshint browser: false, node:true, strict:false */

(function()
    {
        "use strict";
        
        var Syslog = new Class(
            {
                Implements: [Events, 
                        Options], 
                // Options
                Options: {
                    // Default Facility
                    DefaultFacility: 'local0', 
                    // Default Severity
                    DefaultSeverity: 'debug', 
                    // Default Application
                    DefaultApplication: null, 
                    // SyslogVersion
                    SyslogVersion: '1', 
                    // The Facilities
                    Facilities: {'kern': 0, 
                                    'user': 1, 
                                    'mail': 2, 
                                    'daemon': 3, 
                                    'auth': 4, 
                                    'syslog': 5, 
                                    'lpr': 6, 
                                    'news': 7, 
                                    'uucp': 8, 
                                    'clock': 9, // Or 15
                                    'sec': 10, 
                                    'ftp': 11, 
                                    'ntp': 12, 
                                    'audit': 13, 
                                    'alert': 14, 
                                    //'clock': 15, // Clock
                                    'local0': 16, 
                                    'local1': 17, 
                                    'local2': 18, 
                                    'local3': 19, 
                                    'local4': 20, 
                                    'local5': 21, 
                                    'local6': 22, 
                                    'local7': 23
                                }, 
                    // The Severities
                    Severities: {'emerg': 0, 
                                    'emergency': 0, 
                                    'alert': 1, 
                                    'crit': 2, 
                                    'critical': 2, 
                                    'err': 3, 
                                    'error': 3, 
                                    'warn': 4, 
                                    'warning': 4, 
                                    'notice': 5, 
                                    'info': 6, 
                                    'information': 6, 
                                    'informational': 6, 
                                    'debug': 7
                                }
                }, 
                
                // The Log object
                Log: {
                        // Syslog Version
                        Version: 1, 
                        // Facility
                        Facility: '', 
                        // Severity
                        Severity: '', 
                        // TimeStamp
                        TimeStamp: new Date(), 
                        // Application
                        Application: '', 
                        // ProcessId
                        PID: null, 
                        // Message Id
                        MessageId: '', 
                        // Structure Data
                        StructuredData: {}, 
                        // The Message
                        Message: ''
                    }, 
                
                /*
                 * Constructor.
                 * 
                 * @param {Object} Options for the class
                 * @param {String} Existing log data.
                 * @access public
                 */
                __Constructor: function(Options, Data)
                    {
                        
                        if(Data !== undefined)
                            {
                                this.Parse(Data);
                            }
                    }, 
                
                /*
                 * Gets the Facility
                 * 
                 * @return {String} The Facility name
                 * @access public
                 */
                GetFacility: function()
                    {
                        return this.Log.Facility;
                    }, 
                
                /*
                 * Set the Facility
                 * 
                 * @param {String} The Facility from where the log is generated
                 * @return {Object} This class
                 * @access public
                 */
                SetFacility: function(Facility)
                    {
                        if(Facility === undefined || Facility === null)
                            {
                                Facility = this.Options.DefaultFacility;
                            }
                        this.Log.Facility = Facility;
                        return this;
                    },
                
                /*
                 * Gets the Severity
                 * 
                 * @return {String} The Severity
                 * @access public
                 */
                GetSeverity: function()
                    {
                        return this.Log.Severity;
                    }, 
                
                /*
                 * Sets the Severity.
                 * 
                 * @param {String} The Severity name
                 * @return {Object} This class instance
                 * @access public
                 */
                SetSeverity: function(Severity)
                    {
                        if(Severity === undefined || Severity === null)
                            {
                                Severity = this.Options.DefaultSeverity;
                            }
                        this.Log.Severity = Severity;
                        return this;
                    }, 
                
                /*
                 * Gets the TimeStamp
                 * 
                 * @param {String} The Date format (default ISO)
                 * @return {String} The Date
                 * @access public
                 */
                GetTimeStamp: function(Format)
                    {
                        
                    }, 
                
                /*
                 * Sets the TimeStamp
                 * 
                 * @param {Mixed} Date String or Date Object
                 * @param {String} The Date format
                 * @return {Object} This class instance
                 * @access public
                 */
                SetTimeStamp: function(TimeStamp, Format)
                    {
                        
                        return this;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                GetApplication: function()
                    {
                        return this.Log.Application;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return {Object} This class instance
                 * @access public
                 */
                SetApplication: function(Application)
                    {
                        if(Application === undefined)
                            {
                                Application = null;
                            }
                        this.Log.Application = Application.toString().trim() || null;
                        return this;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                GetPID: function()
                    {
                        return this.Log.PID;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                SetPID: function(PID)
                    {
                        PID = parseInt(PID, 10) || null;
                        this.Log.PID = PID;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                GetMessageId: function()
                    {
                        return this.Log.MessageId;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                SetMessageId: function(MessageId)
                    {
                        if(MessageId === undefined)
                            {
                                MessageId = '';
                            }
                        this.Log.MessageId = MessageId.trim().replace(/\[/, '').replace(/\]/, '').replace(/\"/, '').replace(/\'/, '');
                        return this;
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                GetStructuredData: function(Id)
                    {
                        
                    }, 
                
                /*
                 * Sets a structure data.
                 * 
                 * @param {String} Id the SD-ID
                 * @param {Object} The SD Param Value data
                 * @return true/false
                 * @access public
                 */
                SetStructuredData: function(Id, Data)
                    {
                        if(Id === undefined || Data === undefined)
                            {
                                return;
                            }
                        if(typeOf(Data) !== 'object')
                            {
                                return new Error('StructuredData: Data must be an object of key value pair.');
                            }
                        Id = Id.trim();
                        var Tmp = null;
                        switch(Id)
                            {
                                case 'timeQuality': 
                                    // tzKnown (1 if known, else 0), isSynced (1, 0), isSynced (int)
                                    if(this.Log.StructuredData.timeQuality !== undefined)
                                        {
                                            this.Log.StructuredData.timeQuality = {};
                                        }
                                    if(Data.tzKnown !== undefined)
                                        {
                                            Tmp = parseInt(Data.txKnown, 10) || null;
                                            if(Tmp >= 0 || Tmp <= 1)
                                                {
                                                    this.Log.StructuredData.timeQuality.tzKnown = Tmp;
                                                }
                                        }
                                    if(Data.isSynced !== undefined)
                                        {
                                            Tmp = parseInt(Data.isSynced, 10) || null;
                                            if(Tmp >= 0 && Tmp <= 1)
                                                {
                                                    this.Log.StructuredData.timeQuality.isSynced = Tmp;
                                                }
                                        }
                                    if(Data.isSynced !== undefined)
                                        {
                                            Tmp = parseInt(Data.isSynced, 10) || null;
                                            if(Tmp >= 0)
                                                {
                                                    this.Log.StructuredData.timeQuality.isSynced = Tmp;
                                                }
                                        }
                                    // Check if length > 0
                                    if(Object.getLength(this.Log.StructuredData.timeQuality) === 0)
                                        {
                                            delete this.Log.StructuredData.timeQuality;
                                        }
                                    break;
                                case 'origin': 
                                    // Ip (multiple as csv or multiple params), enterpriseId (the enterprise id), software (48 chars, needs enterpriseId), swVersion (32 chars, needs software)
                                    if(this.Log.StructuredData.origin !== undefined)
                                        {
                                            this.Log.StructuredData.origin = {};
                                        }
                                    if(Data.ip !== undefined)
                                        {
                                            // If CSV, convert to array, loop through, verify if valid ip then dump
                                            Data.ip = Data.ip.split(',');
                                            this.Log.StructuredData.origin.ip = [];
                                            Data.ip.each(function(IP)
                                                {
                                                    this.Log.StructuredData.origin.ip.push(IP);
                                                }.bind(this));
                                        }
                                    if(Data.enterpriseId !== undefined)
                                        {
                                            
                                            if(Data.software !== undefined)
                                                {
                                                    
                                                    if(Data.swVersion !== undefined)
                                                        {
                                                            
                                                        }
                                                }
                                        }
                                    
                                    // Check if length > 0
                                    if(Object.getLength(this.Log.StructuredData.origin) === 0)
                                        {
                                            delete this.Log.StructuredData.origin;
                                        }
                                    break;
                                case 'meta': 
                                    // sequenceId (integer, max of 2147483647), sysUpTime (int)
                                    
                                    
                                    // Check if length > 0
                                    if(Object.getLength(this.Log.StructuredData.meta) === 0)
                                        {
                                            delete this.Log.StructuredData.meta;
                                        }
                                    break;
                                default: 
                                    // Check if it is custom, i.e [a-z0-9]{1,}@[0-9]{1,}
                                    if(Id.test(/^[a-z0-9]{1,}@[0-9]{1,}$/i))
                                        {
                                            
                                        }
                                    // Check if length > 0
                                    if(Object.getLength(this.Log.StructuredData[Id]) === 0)
                                        {
                                            delete this.Log.StructuredData.Id;
                                        }
                                    break;
                            }
                        
                    }, 
                
                /*
                 * Gets the log message
                 * 
                 * @return {String} The Log message
                 * @access public
                 */
                GetMessage: function()
                    {
                        return this.Log.Message;
                    }, 
                
                /*
                 * Sets the Log Message
                 * 
                 * @param {String} The Message
                 * @return {Object} This class instance
                 * @access public
                 */
                SetMessage: function(Message)
                    {
                        if(Message === undefined)
                            {
                                Message = '';
                            }
                        this.Log.Message = Message.trim();
                    }, 
                
                /*
                 * Parses a log entry as per RFC5424
                 * 
                 * @param {String} The Log Entry
                 * @param {Object} The SD Param Value data
                 * @return {Object} This class Instance
                 * @access public
                 */
                Parse: function(Data)
                    {
                        
                        return this;
                    }, 
                
                /*
                 * Converts the log Message as aper RFC5424.
                 * 
                 * @return {String} The Log entry
                 * @access public
                 */
                ToString: function()
                    {
                        return '<' + this._CalculatePriority(this.Log.Facility, this.Log.Severity) + '>' 
                                + this.Log.SyslogVersion + ' ' 
                                + this.Log.TimeStamp + ' ' 
                                + this.Log.Hostname + ' '
                                + this.Log.Application + ' '
                                + this.Log.PID + ' '
                                + this.Log.MessageId + ' '
                                + this.__GenerateStructuredData() + ' '
                                + this.Log.Message;
                    }, 
                
                /*
                 * Calculates the priority from Facility and Priority
                 * 
                 * @param {Numeric} The Facility number
                 * @param {Numeric} The Severity number
                 * @return {Numeric} The Priority number
                 * @access private
                 */
                __CalculatePriority: function(Facility, Severity)
                    {
                        return ((parseInt(Facility, 10) || 0) * 8) + (parseInt(Severity, 10) || 0);
                    }.protect(), 
                
                /*
                 * Gets the Facility from the Priority
                 * 
                 * @param {Numeric} The Priority
                 * @return {String} The Facility
                 * @access private
                 */
                __GetFacility: function(Priority)
                    {
                        return parseInt(((parseInt(Priority, 10) || 0) / 8), 10);
                    }.protect(), 
                
                /*
                 * Gets the Severity from the Priority
                 * 
                 * @param {Numeric} The Priority
                 * @return {String} The Severity
                 * @access private
                 */
                __GetSeverity: function(Priority)
                    {
                        return (parseInt(Priority, 10) || 0) - this.__GetFacility(Priority);
                    }.protect(), 
                
                /*
                 * Gets the Severity from the Priority
                 * 
                 * @return {String} The Structured Data as per RFC5424
                 * @access private
                 */
                __GenerateStructuredData: function()
                    {
                        var SD = '';
                        this.Log.StructuredDate.each(function(Data, Id)
                            {
                                SD += '[' + Id;
                                Data.each(function(Value, Key)
                                    {
                                        SD += ' ' + Key + '="' + Value + '"';
                                    }.bind(this));
                                SD += ']';
                            }.bind(this));
                        if(SD.trim().length === 0)
                            {
                                SD = '-';
                            }
                        return SD;
                    }.protect()
            });
        
        module.exports = Syslog;
    })();